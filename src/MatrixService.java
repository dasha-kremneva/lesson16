package src;

public class MatrixService {

    int matrix[][] = new int [10000][10000];

    public int sum(){
        int sum = 0;
        for (int i=0; i < 10000; i++){
            for(int j=0; j < 10000; j++){
                matrix[i][j] = (int)(Math.random() * 10);
                sum += matrix[i][j];
            }
        }
        return sum;
    }


    public static void main(String[]args){
        MatrixService matrixService = new MatrixService();
        System.out.println("Сумма всех элементов матрицы = " + matrixService.sum());
    }
}
